import React from "react";
import Flag from "./flag";
import { ColumnChart } from "react-chartkick";
const xTitle = "Age";
const yTitle = "% Mortality";
import Chart from "chart.js";

const MortalityListItem = ({ mortality }) => {
  const formatedDataMale = formatMortalityData(mortality.male);
  const formatedDataFemale = formatMortalityData(mortality.female);

  return (
    <tr>
      <td>
        <Flag country={mortality.country} className="flag_medium" />
      </td>
      <td className="col-md-6">
        <ColumnChart
          xtitle={xTitle}
          ytitle={yTitle}
          data={formatedDataMale}
          max={30}
        />
      </td>
      <td className="col-md-6">
        <ColumnChart
          xtitle={xTitle}
          ytitle={yTitle}
          data={formatedDataFemale}
          max={30}
        />
      </td>
    </tr>
  );
};

function formatMortalityData(mortality) {
  const filterData = mortality.filter(data => {
    if (data.age >= 101) {
      return false;
    } else {
      return data;
    }
  });
  const array = filterData.map(data => {
    return [
      `${data.age.toFixed(0)}`,
      Number(data.mortality_percent.toFixed(0))
    ];
  });
  return array;
}

export default MortalityListItem;
